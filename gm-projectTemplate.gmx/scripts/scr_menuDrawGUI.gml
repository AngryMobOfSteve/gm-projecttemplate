///scr_menuDrawGUI

if (menuState == menu.disabled)
    exit;

var _dispwidth = display_get_gui_width();
var _dispheight = display_get_gui_height();

var _centerX = _dispwidth / 2;
var _centerY = _dispheight / 2;



if (objGame.gameState != game.title) {
    draw_rectangle_colour(_centerX - (width /2), _centerY - (height/2),
                      _centerX + (width / 2), _centerY + (height/2),
                      c_dkgray, c_dkgray, c_dkgray, c_dkgray, false);
}
//get Text array
var _text = scr_menuText(menuState);
draw_set_halign(fa_center);

draw_text_colour(_centerX, _centerY - (targetHeight / 2) + 16, _text[0], 
                 c_black, c_black, c_black, c_black, 1);

for ( var i = 1; i < array_length_1d(_text); i++)
{
    var _drawColor = c_black;
    if (i == optionCounter)
    {
        _drawColor = c_white;
    }
    draw_text_colour(_centerX, _centerY + (i * 16), _text[i] + scr_menuShowSubOptions(menuState, i), 
                 _drawColor, _drawColor, _drawColor, _drawColor, 1);   
}
