///scrGetAxis(stick, returnType(direction or distance))

var axis = argument0;
var return_string = argument1;

switch(axis)
{
    case "right":
        vert_axis = gp_axisrv;
        hor_axis = gp_axisrh;
    break;
    case "left":
    default:
        vert_axis = gp_axislv;
        hor_axis = gp_axislh;
    break;
}

var xstick = 0;
var ystick = 0;
var stickdir = -1;
var dist = 0;


    
if global.useGamepad 
{

    xstick = gamepad_axis_value(global.gpSlot, hor_axis);
    ystick = gamepad_axis_value(global.gpSlot, vert_axis);   
    
    if (abs(point_distance(0, 0, xstick, ystick) > 0.2 ))
    {
       stickdir = point_direction(0, 0, xstick, ystick);    
    }

    if (abs(point_distance(0, 0, xstick, ystick) > 0.2 ))
    dist = point_distance(0,0, xstick, ystick)

}

if (return_string == "dir") || (return_string == "direction")
{
    return stickdir;
}
else if (return_string == "dist" || (return_string == "distance") )
{
    return dist;
}
else
    return -1;



