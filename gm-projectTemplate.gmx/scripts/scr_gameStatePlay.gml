///scr_gameStatePlay
if (!global.isPaused) {

    if scr_getInput("GPPAUSE", "KBCANCEL", button.pressed) {
        global.isPaused = true;
        if instance_exists(objMenu) && ( objMenu.menuState == menu.disabled)
        {
            objMenu.menuState = menu.pause;    
            objMenu.prevState = menu.disabled;
        }
        exit;
    }

}
else
{
    if scr_getInput("GPPAUSE", "KBCANCEL", button.pressed) {
        global.isPaused = false;
        if (instance_exists(objMenu) && ( objMenu.menuState == menu.pause))
        {
            objMenu.menuState = menu.disabled;
            objMenu.prevState = menu.pause;            
            exit;
        }
    }
}

