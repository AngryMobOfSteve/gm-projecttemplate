///scrGetInput(BUTTON, KEY, QUERY_STATE)

var gp = global.dmap_input[? argument0];
var kb = global.dmap_input[? argument1];
var _state = argument2;
if (global.useGamepad) && (gp != -1)
{
    switch(_state){
        case button.pressed:
            if gamepad_button_check_pressed(global.gpSlot, gp)
                return true;
        break;
        case button.released:
            if gamepad_button_check_released(global.gpSlot, gp)
                return true;
        break;
        case button.down:
            if gamepad_button_check(global.gpSlot, gp)        
                return true;
        break;
        case button.up:
            if !gamepad_button_check(global.gpSlot, gp)  
                return true;
        break;
        case button.state: {
            if gamepad_button_check_pressed(global.gpSlot, gp)
                return button.pressed;
            if gamepad_button_check_released(global.gpSlot, gp)
                return button.released;
            if gamepad_button_check(global.gpSlot, gp)
                return button.down;        
    
            else
                return button.up;        
        }
        break;
    }
}
else
{
    switch(kb)
    {
        case mb_left:
        case mb_middle:
        case mb_right:
            if (_state == button.state)
            {
                if mouse_check_button_pressed(kb)
                    return button.pressed;
                if mouse_check_button_released(kb)
                    return button.released;
                if mouse_check_button(kb) 
                    return button.down; 
                else
                    return button.up;       
            }
            else {
                switch(_state)
                {
                    case button.pressed:
                        if mouse_check_button_pressed(kb)
                            return true;                
                   break;
                   case button.released:
                       if mouse_check_button_released(kb)
                           return true;
                   break;
                   case button.down:
                       if mouse_check_button(kb)
                           return true;
                   break;
                   case button.up:
                       if !mouse_check_button(kb)
                           return true;
                }                    
            }   
        break;
        default:
            if (_state == button.state) {
                if keyboard_check_pressed(kb)
                       return button.pressed;
                if keyboard_check_released(kb)
                   return button.released;
                if keyboard_check(kb)               
                   return button.down; 
                else
                   return button.up;
            }
            else {
                switch(_state)
                {
                    case button.pressed:
                        if keyboard_check_pressed(kb)
                            return true;                
                   break;
                   case button.released:
                       if keyboard_check_released(kb)
                           return true;
                   break;
                   case button.down:
                       if keyboard_check(kb)
                           return true;
                   break;
                   case button.up:
                       if !keyboard_check(kb)
                           return true;
                   break;
                }
            }           
        break;
    }
}




