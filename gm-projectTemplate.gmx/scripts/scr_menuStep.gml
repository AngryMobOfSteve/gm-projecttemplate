///scr_menuStep

var _confirm = (scr_getInput("GPCONFIRM", "KBCONFIRM", button.pressed) | scr_getInput("GPPAUSE", "KBCONFIRM2", button.pressed));
var _cancel = scr_getInput("GPCANCEL", "KBCANCEL", button.pressed);
var _down = scr_getInput("GPDOWN", "KBDOWN", button.pressed) | keyboard_check_pressed(vk_down)
var _up = scr_getInput("GPUP", "KBUP", button.pressed) | keyboard_check_pressed(vk_up)
var _left = scr_getInput("GPLEFT", "KBLEFT", button.pressed)| keyboard_check_pressed(vk_left);
var _right = scr_getInput("GPRIGHT", "KBRIGHT", button.pressed) | keyboard_check_pressed(vk_right);
var _vertInput = _down - _up;
var _horiInput = _right - _left;
var _options = scr_menuText(menuState);
optionCounter = clamp(optionCounter, 1, array_length_1d(_options)); 

switch(menuState)
{
    case menu.disabled:
    break;
    case menu.title:
        targetHeight = 0;
        targetWidth = 0;    
        if (objGame.gameState == game.title)
        {
            if(_confirm)
            {
                scr_menuChangeState(menu.start);
                prevState = menu.title;
            }
            if(_cancel)
            {
                scr_menuChangeState(menu.quit);
                prevState = menu.title;
            }
        }
        else
            exit;
    break;
    case menu.start:
        targetHeight = 0;
        targetWidth = 0;
        var _options = scr_menuText(menuState);
        optionCounter = clamp(optionCounter, 1, array_length_1d(_options));   
        
        if (_vertInput != 0)
        {
            optionCounter += _vertInput;
            if(optionCounter = array_length_1d(_options))
                optionCounter = 1;
            if(optionCounter < 1)
                optionCounter = array_length_1d(_options) - 1;              
        }
        if (_confirm) 
        {          
             switch(_options[optionCounter])
             {             
                 case "PLAY":
                     scr_gameChangeState(game.load);
                     scr_menuChangeState(menu.disabled);  
                 break;
                 case "SETTINGS":
                     prevState = menu.start;
                     scr_menuChangeState(menu.settings);
                     exit;
                 case "QUIT":
                     prevState = menu.start;
                     scr_menuChangeState(menu.quit);
                     exit;
                 break;
             }
        }
        if (_cancel)
        {
            scr_menuChangeState(menu.title);
        }
        
    break;    
    case menu.settings:

        targetHeight = 120;
        targetWidth = 240;
        var _options = scr_menuText(menuState);
        optionCounter = clamp(optionCounter, 1, array_length_1d(_options));   

        if (_vertInput != 0)
        {
            optionCounter += _vertInput;
            if(optionCounter = array_length_1d(_options))
                optionCounter = 1;
            if(optionCounter < 1)
                optionCounter = array_length_1d(_options) - 1;              
        }
        

        if (_confirm) 
        {          
            switch(_options[optionCounter])
            {
                case "GAMEPLAY":
                    scr_menuChangeState(menu.gameplay);
                break;
                case "VISUAL":
                    scr_menuChangeState(menu.visual);
                break;
                case "AUDIO":
                    scr_menuChangeState(menu.audio);
                break;
                case "CONTROLS":
                    scr_menuChangeState(menu.controls);
                break;
            }
        }        
        if (_cancel)
        {
            scr_menuChangeState(prevState);            
            //prevState = menu.settings;
        }
    break;
    case menu.gameplay:
    case menu.visual:
    case menu.audio:
    case menu.controls:
        
        if (_horiInput != 0)
        {
             if (menuState == menu.visual)    
             {
                 if (_options[optionCounter] == "ASPECT RATIO")
                 {
                     scr_displayChangeAspect(_horiInput);
                 }
                 else if (_options[optionCounter] == "RESOLUTION")
                 {
                     scr_displayChangeZoom(_horiInput);
                 }
                 else if (_options[optionCounter] == "FULLSCREEN")
                 {
                     var _full = window_get_fullscreen();
                     window_set_fullscreen(!window_get_fullscreen());
                 }
             }

        }
        
        
        if (_vertInput != 0)
        {
            optionCounter += _vertInput;
            if(optionCounter = array_length_1d(_options))
                optionCounter = 1;
            if(optionCounter < 1)
                optionCounter = array_length_1d(_options) - 1;              
        }    
        if (_cancel)
        {
            scr_menuChangeState(menu.settings);            

        }    
    break;
    case menu.pause:
        targetHeight = 80;
        targetWidth = 240;                
        var _options = scr_menuText(menuState);
        optionCounter = clamp(optionCounter, 1, array_length_1d(_options));   

        if (_vertInput != 0)
        {
            optionCounter += _vertInput;
            if(optionCounter = array_length_1d(_options))
                optionCounter = 1;
            if(optionCounter < 1)
                optionCounter = array_length_1d(_options) - 1;              
        }
        if (_confirm) 
        {          
            if (_options[optionCounter] == "SETTINGS")
            {                    
                prevState = menuState;             
                scr_menuChangeState( menu.settings);               
                exit;
            }
            else if (_options[optionCounter] == "QUIT")
            {
                prevState = menuState;
                scr_menuChangeState( menu.quit);

                exit;
            }
            
        if (_cancel)
            {
                scr_menuChangeState(menu.disabled);
            }
        }                  
    break;
    case menu.quit:
        targetHeight = 80;
        targetWidth = 120;         
        var _options = scr_menuText(menuState);
        optionCounter = clamp(optionCounter, 1, array_length_1d(_options));                     
        
        if (_vertInput != 0)
        {
            optionCounter += _vertInput;
            if(optionCounter = array_length_1d(_options))
                optionCounter = 1;
            if(optionCounter < 1)
                optionCounter = array_length_1d(_options) - 1;              
        }
        if (_confirm) 
        {            
            if (_options[optionCounter] == "YES")
            {
                //TODO: Find a better way of doing this. 
                if (objGame.gameState == game.title)
                    scr_gameChangeState( game.quit);
                else {
                    scr_gameChangeState(game.title);
                    scr_menuChangeState(menu.start);
                }
            }
            else
            {
                scr_menuChangeState(prevState);
            }
        }
        if (_cancel)
        {
             scr_menuChangeState(prevState);
        }
        
    break;

}



height = lerp(height, targetHeight, speeds.menuPop);
width = lerp(width, targetWidth, speeds.menuPop);
