///scr_init

randomize();
global.debug = true;
global.fastQuit = false;
global.isPaused = false;
global.useGamepad = 0;
global.gpSlot = -1;
scr_checkGamepad();
global.dmap_input = ds_map_create();
global.dmap_input = scr_setupInput();
global.aspectRatioString = "";
global.ratio[3] = "21:9";
global.ratio[2] = "16:10";
global.ratio[1] = "16:9";
global.ratio[0] = "4:3";
