enum speeds {
    menuPop = 1,
    length
}

enum game {
    rez,
    title,
    attract,
    load,
    play,
    pause,
    gameover,
    quit,
    edit
}

enum menu {
    disabled,
    title,
    start,
    settings,
    gameplay,
    visual,
    audio,
    controls,    
    pause,
    debug,
    quit
}

enum settings {
    settings,
    gameplay,
    visual,
    audio,
    controls,     
    length
}


enum difficulty {
    easy,
    medium,
    hard
}

enum visual {
     visual,
     aspect_ratio,
     resolution,
     fullscreen,
     borderless
}

enum audio {
    master,
    music,
    sfx
}

enum controls {
    gamepad,
    rebind
}
     
enum button {
     released = -1,
     up = 0,
     down = 1,
     pressed = 2,
     state = 3
}
