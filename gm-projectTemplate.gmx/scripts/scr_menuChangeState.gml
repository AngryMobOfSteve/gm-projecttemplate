///scr_menuChangeState(NEXT_STATE)
var _next = argument0;

with(objMenu)
{
    menuState = _next;
    //reset option Counter here so I don't have to do it everywhere
    optionCounter = 1;     

}
