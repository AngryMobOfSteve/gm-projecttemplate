///scrSetupInput

var ds_input = ds_map_create();
ds_input[? "null"] = -1;
ds_input[? "gpslot"] = global.gpSlot;
ds_input[? "GPWPNTOGGLE"] = gp_face4;
ds_input[? "GPMODESPECIAL"] = gp_shoulderl;
ds_input[? "GPMODE"] = gp_shoulderlb;
ds_input[? "GPOPTION"] = gp_shoulderl;
ds_input[? "GPSPECIAL"] = gp_shoulderr;
ds_input[? "GPFIRE"] = gp_shoulderrb;
ds_input[? "GPPAUSE"] = gp_start;
ds_input[? "GPCONFIRM"] = gp_face1;
ds_input[? "GPCANCEL"] = gp_face2;
ds_input[? "GPEXIT"] = gp_select;
ds_input[? "GPLEFT"] = gp_padl; 
ds_input[? "GPRIGHT"] = gp_padr;
ds_input[? "GPUP"] = gp_padu;
ds_input[? "GPDOWN"] = gp_padd;
ds_input[? "KBPAUSE"] = vk_tab;
ds_input[? "KBCONFIRM"] = vk_enter;
ds_input[? "KBCANCEL"] = vk_escape;
ds_input[? "KBLEFT"] = ord("A");
ds_input[? "KBRIGHT"] = ord("D");
ds_input[? "KBDOWN"] = ord("S");
ds_input[? "KBUP"] = ord("W");
ds_input[? "KBFIRE"] = mb_left;
ds_input[? "KBSPECIAL"] = mb_right;
ds_input[? "KBOPTION"] = mb_middle;
ds_input[? "KBMODE" ] = vk_space;
ds_input[? "KBMODESPECIAL" ] = vk_lshift;
ds_input[? "KBWPNTOGGLE"] = vk_lalt;
ds_input[? "KBCONFIRM2" ]= vk_space;
return ds_input;




