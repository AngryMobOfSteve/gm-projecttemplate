///scr_displayChangeAspect(ADD_VALUE)
var _addValue = argument0;
with (objDisplayManager)
{
    if (_addValue != 0)
    {
        global.autoSetAspectRatio = false;
        ar_counter += _addValue;
        if (ar_counter = array_length_1d(dar))
            ar_counter = 0;
        if (ar_counter < 0)
            ar_counter = array_length_1d(dar) - 1;            
        settingsChanged = true;    
        show_debug_message(ar_counter);

    }
    


}
